import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LoginComponent } from './login';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'menu', component: MainNavComponent,
   children: [
      { path: 'personajes',
            loadChildren: () => import('./components/personajes/personajes.module').then(m => m.PersonajesModule) },
      { path: 'profesores',
            loadChildren: () => import('./components/profesores/profesores.module').then(m => m.ProfesoresModule) },
      { path: 'estudiantes',
            loadChildren: () => import('./components/estudiantes/estudiantes.module').then(m => m.EstudiantesModule) },
      { path: 'form-estudiante',
            loadChildren: () => import('./components/form-estudiante/form-estudiante.module').then(m => m.FormEstudianteModule) },
      { path: 'directivas',
            loadChildren: () => import('./components/directivas/directivas.module').then(m => m.DirectivasModule) },
      { path: 'grafico',
            loadChildren: () => import('./components/grafico/grafico.module').then(m => m.GraficoModule) },
      { path: 'stepper',
            loadChildren: () => import('./components/stepper/stepper.module').then(m => m.StepperModule) },
      { path: 'text-mask-addons',
            loadChildren: () => import('./components/text-mask-addons/text-mask-addons.module').then(m => m.TextMaskAddonsModule) },      
   ],
  },
  { path: '**', loadChildren: () => import('./components/not-found/not-found.module').then(m => m.NotFoundModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
