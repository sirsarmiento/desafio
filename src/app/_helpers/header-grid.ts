export const headerGrid =  [
    {headerName: 'Name', field: 'name', width: 210, sortable: true, filter: true, resizable: true,
     checkboxSelection: function (params: any) {
       return params.columnApi.getRowGroupColumns().length === 0;
   },
    },
    {headerName: 'Patronus', field: 'patronus', width: 110, sortable: true, filter: true, resizable: true, },
    {headerName: 'Age', field: 'dateOfBirth', width: 90, sortable: true, filter: true, resizable: true },
    {headerName: 'Image', field: 'image', width: 350 }
  ];
