import { Component, OnInit } from '@angular/core';
import { MenuItem } from './menu-item';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  rolModules: any[] = [];
  menuDinamic: any[] = [];
  subMenuDinamic: any[] = [];
  constructor() { }

  menuItems: MenuItem[] = [
    {
      name: 'Desafio', permissionName: '', icon: 'menu', route: '', items: [
        { name: 'Personajes', permissionName: 'objetives', icon: '', route: 'personajes', items: [] },
        { name: 'Estudiantes', permissionName: 'objetives', icon: '', route: 'estudiantes', items: [] },
        { name: 'Profesores', permissionName: 'objetives', icon: '', route: 'profesores', items: [] },
        { name: 'Estudiante', permissionName: 'objetives', icon: '', route: 'form-estudiante', items: [] },
      ]
    },
    {
      name: 'Utilidades', permissionName: '', icon: 'menu', route: '', items: [
        { name: 'Directivas', permissionName: 'objetives', icon: '', route: 'directivas', items: [] },
        { name: 'Graficos', permissionName: 'objetives', icon: '', route: 'grafico', items: [] },
        { name: 'Stepper', permissionName: 'objetives', icon: '', route: 'stepper', items: [] },
        { name: 'Text-mask-addons', permissionName: 'objetives', icon: '', route: 'text-mask-addons', items: [] }
      ]
    }
  ];  

  ngOnInit(): void {
  }

  showMenuItem(menuItem): boolean {
    return true;
  }

  /**
   * Show submenu options from the selected menu
   * @param index selected menu index
   */
  showSubMenu(index: number) {
    if (this.menuItems[index].showSubMenu) {
      this.menuItems[index].showSubMenu = false;
    } else {
      this.menuItems[index].showSubMenu = true;
    }
  }

}
