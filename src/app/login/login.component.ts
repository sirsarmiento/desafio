﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../_services';
import { GlobalsService } from '../_services/globals.service';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';

@Component({
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.css']
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private user: GlobalsService
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['ssarmiento', Validators.required],
            password: ['test50*', Validators.required]
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/menu';
    }

    // convenience getter for easy accemessageelds
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalidmessage
        if (this.loginForm.invalid) {
            return;
        }

        this.router.navigate([this.returnUrl]);

        this.user.user = this.f.username.value;

        // this.authenticationService.login(this.f.username.value, this.f.password.value)
        // .pipe(first())
        // .subscribe(
        //     data => {
        //         this.router.navigate([this.returnUrl]);
        //         this.user.user = this.f.username.value;
        //     },
        //     error => {
        //         this.error = error;
        //         this.loading = false;
        //         console.log(error);
        //     });
    }
}
