import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataApiService {
  constructor(private http: HttpClient) {    }

  url_api = "http://hp-api.herokuapp.com/api/characters/";

  /**
    * Lista de personajes por casa de hechicería.
    */
  getCharacters(id:any){
    const characters = this.url_api + "house/" + id;
    return this.http.get(characters);
  }

  /**
    * Lista de estudiantes
    */
  getStudents(){
    const students = this.url_api + "students/";
    return this.http.get(students);
  }

   /**
    * Lista de profesores
    */
  getTeachers() {
    const teachers = this.url_api + 'staff/';
    return this.http.get(teachers);
  }
}
