import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { FormEstudianteRoutingModule } from './form-estudiante-routing.module';
import { FormEstudianteComponent } from './form-estudiante.component';


@NgModule({
  declarations: [FormEstudianteComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FormEstudianteRoutingModule
  ]
})
export class FormEstudianteModule { }
