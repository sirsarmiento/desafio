import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataApiService } from '../../_services/data-api.service';
import { HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-form-estudiante',
  templateUrl: './form-estudiante.component.html',
  styleUrls: ['./form-estudiante.component.css']
})
export class FormEstudianteComponent implements OnInit {
  rowData: any[] = [];

  studentForm: FormGroup;
  submitted = false;
  loading = false;
  postData: object = {};
  name = '';
  patronus = '';
  image = '';
  age = '';

  url = 'http://apiTest/newStudent';

  constructor(
    private formBuilder: FormBuilder,
    private dataApi: DataApiService,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.studentForm = this.formBuilder.group({
          name: ['', Validators.required],
          patronus: ['', Validators.required],
          age: ['', Validators.required],
          image: ['', Validators.required]
      });
  }

  // convenience getter for easy accemessageelds
  get f() { return this.studentForm.controls; }

  onSubmit() {
    this.submitted = true;
      // stop here if form is invalidmessage
    if (this.studentForm.invalid) {
        return;
    }

    this.postData = {
      'name': this.f.name.value,
      'patronus': this.f.patronus.value,
      'dateOfBirth': this.f.age.value,
      'image': this.f.image.value
     };

    // Agregar una solicitud de nuevo estudiante
    // this.http.post(this.url, this.postData).subscribe();

    this.name = this.f.name.value;
    this.patronus = this.f.patronus.value;
    this.age = this.f.age.value;
    this.image = this.f.image.value;

    this.addRow(this.studentForm.value);
  }

  addRow(value) {
    if (this.rowData.length === 0) {
      this.addItem(value);
   } else {
     const result = this.rowData.find(student => student.name === value.name);
     if (result === undefined) {
      this.addItem(value.name);
     } else {
       console.log('Registro existe');
     }
   }
  }

  addItem(value) {
    this.rowData.push (
      { name: value.name, age: value.age }
    );
  }
}
