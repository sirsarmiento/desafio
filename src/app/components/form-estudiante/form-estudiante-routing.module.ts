import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormEstudianteComponent } from './form-estudiante.component';

const routes: Routes = [{ path: '', component: FormEstudianteComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormEstudianteRoutingModule { }
