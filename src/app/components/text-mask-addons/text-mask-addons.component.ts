import { Component, OnInit, Input } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

import createNumberMask from 'text-mask-addons/dist/createNumberMask';


@Component({
  selector: 'app-text-mask-addons',
  templateUrl: './text-mask-addons.component.html',
  styleUrls: ['./text-mask-addons.component.css']
})
export class TextMaskAddonsComponent implements OnInit {

  ngOnInit() {}

}
