import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TextMaskAddonsRoutingModule } from './text-mask-addons-routing.module';
import { TextMaskAddonsComponent } from './text-mask-addons.component';
import { registerLocaleData } from '@angular/common';

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';
import { ReactiveFormsModule } from '@angular/forms';

registerLocaleData(localeDe, 'de', localeDeExtra);

@NgModule({
  declarations: [TextMaskAddonsComponent],
  imports: [
    CommonModule,
    TextMaskAddonsRoutingModule,
    BrowserModule, ReactiveFormsModule, FormsModule
  ]
})
export class TextMaskAddonsModule { }
