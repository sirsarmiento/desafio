import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TextMaskAddonsComponent } from './text-mask-addons.component';

const routes: Routes = [{ path: '', component: TextMaskAddonsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TextMaskAddonsRoutingModule { }
