import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextMaskAddonsComponent } from './text-mask-addons.component';

describe('TextMaskAddonsComponent', () => {
  let component: TextMaskAddonsComponent;
  let fixture: ComponentFixture<TextMaskAddonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextMaskAddonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextMaskAddonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
