import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-grafico',
  templateUrl: './grafico.component.html',
  styleUrls: ['./grafico.component.css']
})
export class GraficoComponent implements OnInit {
  chart;
  capturedImage;
  constructor() { }

  ngOnInit() {
    this.chart = new Chart('canvas', {
      type: 'doughnut',
      data: {
        labels: ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'],
        datasets: [{
          label: 'Mi primer ...',
          data: [350, 450, 340],
          backgroundColor: [
            'rgba(110, 114, 20, 1)',
            'rgba(118, 183, 172, 1)',
            'rgba(0, 148, 97, 1)',
            'rgba(129, 78, 40, 1)',
            'rgba(129, 199, 111, 1)'
        ],
          borderColor: 'red',
          fill: false,
        }]
      },
      options: {
        legend: {
          display: true, position: 'right',
          labels: {
            boxWidth: 10,
            fontSize: 10
          }
        }
      }
    });
  }

  captureCanvas() {
    const canvas = document.getElementById('canvas') as HTMLCanvasElement;
    this.capturedImage = canvas.toDataURL();
    console.log(this.capturedImage);
 }
}
