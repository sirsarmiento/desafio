import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts';
import { GraficoRoutingModule } from './grafico-routing.module';
import { GraficoComponent } from './grafico.component';


@NgModule({
  declarations: [GraficoComponent],
  imports: [
    CommonModule,
    GraficoRoutingModule,
    ChartsModule
  ]
})
export class GraficoModule { }
