import { Component, OnInit, ViewChild } from '@angular/core';
import { DataApiService } from '../../_services/data-api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { HttpClient} from '@angular/common/http';
import { GlobalsService } from '../../_services/globals.service';
import { headerGrid } from '../../_helpers/header-grid';
import { houses } from '../../_helpers/houses';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

export interface User {
  name: string;
}

@Component({
  selector: 'app-personajes',
  templateUrl: './personajes.component.html',
  styleUrls: ['./personajes.component.css']
})
export class PersonajesComponent implements OnInit {
  selectedCharacters: any[];
  houses: any[];
  public rowData: any[];
  character: any;
  name: any;
  patronus: any;
  age: any;
  image: any;
  loading = false;
  activarImp: boolean;
  columnDefs: any[];
  house: '';
  AgLoad: boolean;
  isLoadingOfferAutoComplete: boolean = false;

  constructor(
      private dataApi: DataApiService,
      private spinnerService: NgxSpinnerService,
      private http: HttpClient,
      private user: GlobalsService,
      private router: Router,
    ) {

    this.houses = houses;

    this.activarImp = false;
  }

  ngOnInit() {
    /**
     * Si el usuario no esta logueado lo redirije a la página de inicio de sesión
     */
     if (this.user.user) {
       this.columnDefs = headerGrid;
     } else {
       this.router.navigate(['/login']);
     }
  }

  onChangeofOptions(name: any) {
    this.AgLoad = true;
    this.spinnerService.show();
    this.dataApi.getCharacters(name).subscribe((res: any) => {
     this.rowData = res;
    });

    this.activarImp = true;
    this.house = name;
    this.spinnerService.hide();
  }

  // Se dispara cuando se lecciona el Check

  onSelectionChanged(event: any) {
        const selectedRowsc = event.api.getSelectedRows();
        if (selectedRowsc[0] !== undefined) {
            this.name = selectedRowsc[0].name;
            this.patronus = selectedRowsc[0].patronus;
            this.age = selectedRowsc[0].dateOfBirth;
            this.image = selectedRowsc[0].Image;
            this.activarImp = true;
        } else {
            this.activarImp = false;
        }
   }


  // Se dispara cuando se lecciona la celda
  onCellClicked(event: any) { }

  printPDF() {
    this.spinnerService.show();
    console.log(this.rowData);
    const pdf = {
      pageSize: { width: 595.28, height: 900 },
      content: [
          this.parser(this.rowData)
      ]
    };
    this.spinnerService.hide();
    pdfMake.createPdf(pdf).download(this.rowData[0].house + '.pdf');
  }

  parser(data: any) {
    const columna = [];

    columna.push({"columns": [
      {'text': 'Name', style:{ "alignment": "left", bold: true}},
      {'text': 'Actor', style:{ "alignment": "left", bold: true}},
      {'text': 'Gender', style:{"alignment":"center"}, bold: true},
      {'text': 'House', style:{"alignment":"left"}, bold: true, fontSize: 9, margin: [0, 4]}
    ], width: 208, });

    data.forEach(item => {
      columna.push({'columns': [
        {'text': item.name, style: { 'alignment': 'left'}},
        {'text': item.actor, style: { 'alignment' : 'left'}},
        {'text': item.gender, style: { 'alignment' : 'center'}, fontSize: 9},
        {'text': item.house, style: { 'alignment' : 'left'}, fontSize: 9, margin: [0, 4]}
      ], width: 208, });
    });
    return columna;
  }
}
