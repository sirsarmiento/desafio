import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../_services/data-api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { headerGrid } from '../../_helpers/header-grid';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

import { HttpClient} from '@angular/common/http';
import { GlobalsService } from '../../_services/globals.service';

@Component({
  selector: 'app-estudiantes',
  templateUrl: './estudiantes.component.html',
  styleUrls: ['./estudiantes.component.css']
})
export class EstudiantesComponent implements OnInit {

  rowData: any[];
  character: any;
  name: any;
  patronus: any;
  age: any;
  image: any;
  loading = false;
  activarImp: boolean;
  columnDefs: any[];

  constructor(
      private dataApi: DataApiService,
      private spinnerService: NgxSpinnerService,
      private http: HttpClient,
      private user: GlobalsService,
      private router: Router,
    ) {}


  ngOnInit() {
    /**
     * Si el usuario no esta logueado lo redirije a la página de inicio de sesión
     */
     if (this.user.user) {
      this.columnDefs = headerGrid;
      this.dataApi.getStudents().subscribe((res: any) => {
         this.rowData = res;
      });
     } else {
       this.router.navigate(['/login']);
     }
  }

  // Se dispara cuando se lecciona la celda
  onCellClicked(event: any) { }

  // Se dispara cuando se lecciona el Check
  onSelectionChanged(event: any) {
        const selectedRowsc = event.api.getSelectedRows();
        if (selectedRowsc[0] !== undefined) {
            this.name = selectedRowsc[0].name;
            this.patronus = selectedRowsc[0].patronus;
            this.age = selectedRowsc[0].dateOfBirth;
            this.image = selectedRowsc[0].Image;
            this.activarImp = true;
        } else {
            this.activarImp = false;
        }
  }
}
