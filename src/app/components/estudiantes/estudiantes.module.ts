import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgGridModule } from 'ag-grid-angular';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FormsModule } from '@angular/forms';
import { EstudiantesRoutingModule } from './estudiantes-routing.module';
import { EstudiantesComponent } from './estudiantes.component';


@NgModule({
  declarations: [EstudiantesComponent],
  imports: [
    CommonModule,
    FormsModule,
    EstudiantesRoutingModule,
    AgGridModule.withComponents([]),
    NgxSpinnerModule
  ]
})
export class EstudiantesModule { }
