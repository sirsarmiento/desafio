import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directivas',
  templateUrl: './directivas.component.html',
  styleUrls: ['./directivas.component.css']
})
export class DirectivasComponent implements OnInit {
  clickedOutsideCount = 0; // Prueba clicOutside
  constructor() { }

  ngOnInit() {
  }

 incrementClickOutsideCount() {
   this.clickedOutsideCount += 1;
 }
}
