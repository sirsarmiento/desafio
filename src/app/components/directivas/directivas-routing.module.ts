import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DirectivasComponent } from './directivas.component';

const routes: Routes = [{ path: '', component: DirectivasComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DirectivasRoutingModule { }
