import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DirectivasRoutingModule } from './directivas-routing.module';
import { DirectivasComponent } from './directivas.component';
import { SharedModule } from '../../shared/modules/shared.module';

@NgModule({
  declarations: [DirectivasComponent],
  imports: [
    CommonModule,
    DirectivasRoutingModule,
    SharedModule
  ]
})
export class DirectivasModule { }
