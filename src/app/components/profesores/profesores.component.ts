import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../_services/data-api.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import { headerGrid } from '../../_helpers/header-grid';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import { ButtonRendererComponent } from './../../_helpers/button-renderer.component';

import { HttpClient} from '@angular/common/http';
import { GlobalsService } from '../../_services/globals.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ModalComponent } from './../../components/modal/modal.component';

@Component({
  selector: 'app-profesores',
  templateUrl: './profesores.component.html',
  styleUrls: ['./profesores.component.css']
})
export class ProfesoresComponent implements OnInit {
  rowData: any[];
  character: any;
  name: any;
  patronus: any;
  age: any;
  image: any;
  loading = false;
  activarImp: boolean;
  columnDefs: any[];
  AgLoad: boolean;
  frameworkComponents: any;

  constructor(
    private dataApi: DataApiService,
    private spinnerService: NgxSpinnerService,
    private http: HttpClient,
    private user: GlobalsService,
    private router: Router,
    public matDialog: MatDialog
  ) {
    this.frameworkComponents = {
      buttonRenderer: ButtonRendererComponent,
    };
  }

  ngOnInit() {
 
    this.AgLoad = true;
    this.columnDefs = [
      {headerName: 'Name', field: 'name', width: 140, sortable: true, filter: true, resizable: true, },
      {headerName: 'Patronus', field: 'patronus', width: 100, sortable: true, filter: true, resizable: true, },
      {headerName: 'Age', field: 'dateOfBirth', width: 90, sortable: true, filter: true, resizable: true },
      {headerName: 'Image', field: 'image', width: 320 },
      {headerName: 'Acción', cellRenderer: 'buttonRenderer', width: 90,
        cellRendererParams: {
          onClick: this.openModal.bind(this),
          label: 'Ejecutar'
        }
      }
    ];

    /**
     * Si el usuario no esta logueado lo redirije a la página de inicio de sesión
     */
    if (this.user.user) {
      // this.columnDefs = headerGrid;
      this.dataApi.getTeachers().subscribe((res: any) => {
        this.rowData = res;
      });
    } else {
      this.router.navigate(['/login']);
    }
  }

  onBtnClick1(e) {
    console.log(e.rowData);
  }
  // Se dispara cuando se lecciona el Check
  onSelectionChanged(event: any) {
        const selectedRowsc = event.api.getSelectedRows();
        if (selectedRowsc[0] !== undefined) {
            this.name = selectedRowsc[0].name;
            this.patronus = selectedRowsc[0].patronus;
            this.age = selectedRowsc[0].dateOfBirth;
            this.image = selectedRowsc[0].Image;
            this.activarImp = true;
        } else {
            this.activarImp = false;
        }
   }

  // Se dispara cuando se lecciona la celda
  onCellClicked(event: any) { }

  openModal() {
    const dialogConfig = new MatDialogConfig();
    // The user can't close the dialog by clicking outside its body
    dialogConfig.disableClose = true;
    dialogConfig.id = 'modal-component';
    dialogConfig.height = '350px';
    dialogConfig.width = '600px';
    // https://material.angular.io/components/dialog/overview
    const modalDialog = this.matDialog.open(ModalComponent, dialogConfig);
  }


}
