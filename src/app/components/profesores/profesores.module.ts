import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { FormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { ProfesoresRoutingModule } from './profesores-routing.module';
import { ProfesoresComponent } from './profesores.component';


@NgModule({
  declarations: [ProfesoresComponent],
  imports: [
    CommonModule,
    FormsModule,
    ProfesoresRoutingModule,
    AgGridModule.withComponents([]),
    NgxSpinnerModule
  ]
})
export class ProfesoresModule { }
